use kabegami::{
	draw,
	state
};

fn main()
{
	let mut state = state::State::new().unwrap();
	let mut event_queue = state.get_event_queue().unwrap();

	loop
	{
		event_queue.blocking_dispatch(&mut state).unwrap();
	}
}
