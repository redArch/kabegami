use std::{
	error,
	fs::File,
	io::{
		self,
		Write
	},
	os::unix::io::AsRawFd,
	sync
};

use vulkano::{
	self,
	device::{
		self,
		physical
	},
	instance
};
use wayland_client::{
	protocol::{
		wl_buffer,
		wl_shm
	},
	ConnectionHandle,
	QueueHandle
};

use crate::state;

pub struct Draw
{
	instance: sync::Arc<instance::Instance>,
	device: sync::Arc<device::Device>,
	queue: sync::Arc<device::Queue>
}

impl Draw
{
	pub fn new() -> Self
	{
		let instance_extensions = instance::InstanceExtensions {
			khr_surface: true,
			khr_wayland_surface: true,
			..instance::InstanceExtensions::none()
		};

		// Initialization
		let instance = instance::Instance::new(None, instance::Version::V1_1, &instance_extensions, None)
			.expect("failed to create instance");

		let physical = physical::PhysicalDevice::enumerate(&instance)
			.next()
			.expect("no device available");

		// Device creation
		let queue_family = physical
			.queue_families()
			.find(|&q| q.supports_graphics())
			.expect("couldn't find a graphical queue family");

		let (device, mut queues) = {
			device::Device::new(
				physical,
				&device::Features::none(),
				&device::DeviceExtensions::none(),
				[(queue_family, 0.5)].iter().cloned()
			)
			.expect("failed to create device")
		};

		let queue = queues.next().unwrap();

		Draw {
			instance,
			device,
			queue
		}
	}
}

pub fn draw(tmp: &mut File, (buf_x, buf_y): (i32, i32), progress: &mut u16)
{
	let mut buf = std::io::BufWriter::new(tmp);

	let d = (u16::MAX / 255) as f32;

	let a = 0x0F;

	let r = f32::max(
		(f32::abs(*progress as f32 - (d / 2.0)) as f32 / d as f32 * 765.0) - 127.0,
		63.0
	) as u32;
	let g = f32::max(
		(-1.0 * f32::abs(*progress as f32 - (d / 3.0)) as f32 / d as f32 * 765.0) + 255.0,
		63.0
	) as u32;
	let b = f32::max(
		(-1.0 * f32::abs(*progress as f32 - (d * 2.0 / 3.0)) as f32 / d as f32 * 765.0) + 255.0,
		63.0
	) as u32;

	let color = (a << 24) + (r << 16) + (g << 8) + b;

	for _y in 0..buf_y
	{
		for _x in 0..buf_x
		{
			buf.write_all(&color.to_ne_bytes()).unwrap();
		}
	}

	buf.flush().unwrap();

	if *progress < u16::MAX / 255
	{
		*progress += 1;
	}
	else
	{
		*progress = 0;
	}
}

pub fn create_wl_buffer(
	wl_shm: &wl_shm::WlShm,
	connection_handle: &mut ConnectionHandle,
	queue_handle: &QueueHandle<state::State>,
	file: &mut File,
	output_size: (i32, i32)
) -> Result<wl_buffer::WlBuffer, Box<dyn error::Error>>
{
	let pool = wl_shm.create_pool(
		connection_handle,
		file.as_raw_fd(),
		output_size.0 * output_size.1 * 4,
		queue_handle,
		()
	)?;

	let buffer = pool.create_buffer(
		connection_handle,
		0,
		output_size.0,
		output_size.1,
		output_size.0 * 4,
		wl_shm::Format::Argb8888,
		queue_handle,
		()
	)?;

	Ok(buffer)
}
