use std::{
	error,
	fs::File,
	io::Seek
};

use wayland_client::{
	protocol::{
		wl_buffer,
		wl_callback,
		wl_compositor,
		wl_display,
		wl_output,
		wl_registry,
		wl_shm,
		wl_shm_pool,
		wl_surface
	},
	Connection,
	ConnectionHandle,
	Dispatch,
	EventQueue,
	QueueHandle
};
use wayland_protocols::{
	unstable::xdg_output::v1::client::{
		zxdg_output_manager_v1,
		zxdg_output_v1
	},
	wlr::unstable::layer_shell::v1::client::{
		zwlr_layer_shell_v1,
		zwlr_layer_surface_v1
	},
	xdg_shell::client::xdg_wm_base
};

use crate::draw;

pub struct State
{
	wl_display: wl_display::WlDisplay,
	event_queue: Option<EventQueue<Self>>,
	wl_compositor: Option<wl_compositor::WlCompositor>,
	wl_shm: Option<wl_shm::WlShm>,
	zxdg_output_manager: Option<zxdg_output_manager_v1::ZxdgOutputManagerV1>,
	layer_shell: Option<zwlr_layer_shell_v1::ZwlrLayerShellV1>,
	outputs: Vec<Output>
}

pub struct Output
{
	// this tuple contains a WlBuffer and its associated buffer, plus a bool indicating if the
	// buffer has been released by the compositor
	buffers: Vec<(wl_buffer::WlBuffer, File, bool)>,
	wl_surface: wl_surface::WlSurface,
	layer_shell_surface: zwlr_layer_surface_v1::ZwlrLayerSurfaceV1,
	zxdg_output: zxdg_output_v1::ZxdgOutputV1,
	output_size: Option<(i32, i32)>,
	configured: bool,
	progress: u16,
	wl_callback: Option<wl_callback::WlCallback>
}

impl State
{
	pub fn new() -> Result<Self, Box<dyn error::Error>>
	{
		let connection = Connection::connect_to_env()?;

		let event_queue = connection.new_event_queue();

		let queue_handle = event_queue.handle();

		let wl_display = connection.handle().display();

		wl_display.get_registry(&mut connection.handle(), &queue_handle, ())?;

		Ok(State {
			wl_display,
			event_queue: Some(event_queue),
			wl_compositor: None,
			wl_shm: None,
			zxdg_output_manager: None,
			layer_shell: None,
			outputs: Vec::new()
		})
	}

	pub fn get_event_queue(&mut self) -> Option<EventQueue<Self>>
	{
		self.event_queue.take()
	}
}

// TODO account for different order of events
// will panic if wl_output is sent before zxdg_output_manager
impl Dispatch<wl_registry::WlRegistry> for State
{
	type UserData = ();

	fn event(
		&mut self,
		registry: &wl_registry::WlRegistry,
		event: wl_registry::Event,
		_: &(),
		connection_handle: &mut ConnectionHandle,
		queue_handle: &QueueHandle<Self>
	)
	{
		if let wl_registry::Event::Global {
			name,
			interface,
			version: _
		} = event
		{
			#[cfg(debug_assertions)]
			println!("{:?}", &interface[..]);

			match &interface[..]
			{
				"wl_compositor" =>
				{
					let compositor = registry
						.bind::<wl_compositor::WlCompositor, _>(connection_handle, name, 1, queue_handle, ())
						.unwrap();

					self.wl_compositor = Some(compositor);
				}
				"wl_shm" =>
				{
					let shm = registry
						.bind::<wl_shm::WlShm, _>(connection_handle, name, 1, queue_handle, ())
						.unwrap();
					self.wl_shm = Some(shm);
				}
				"zwlr_layer_shell_v1" =>
				{
					let layer_shell = registry
						.bind::<zwlr_layer_shell_v1::ZwlrLayerShellV1, _>(connection_handle, name, 1, queue_handle, ())
						.unwrap();

					self.layer_shell = Some(layer_shell);
				}
				"zxdg_output_manager_v1" =>
				{
					self.zxdg_output_manager = Some(
						registry
							.bind::<zxdg_output_manager_v1::ZxdgOutputManagerV1, _>(
								connection_handle,
								name,
								1,
								queue_handle,
								()
							)
							.unwrap()
					);
				}
				"wl_output" =>
				{
					let wl_output = registry
						.bind::<wl_output::WlOutput, _>(connection_handle, name, 1, queue_handle, ())
						.unwrap();

					let zxdg_output = self
						.zxdg_output_manager
						.as_ref()
						.unwrap()
						.get_xdg_output(connection_handle, &wl_output, queue_handle, ())
						.unwrap();

					let wl_surface = self
						.wl_compositor
						.as_ref()
						.unwrap()
						.create_surface(connection_handle, queue_handle, ())
						.unwrap();

					let layer_shell = self.layer_shell.as_ref().unwrap();

					// create surface on background layer
					let layer_shell_surface = layer_shell
						.get_layer_surface(
							connection_handle,
							&wl_surface,
							Some(&wl_output),
							zwlr_layer_shell_v1::Layer::Background,
							String::from("Kabegami"),
							queue_handle,
							()
						)
						.unwrap();

					let output = Output {
						buffers: Vec::new(),
						wl_surface,
						layer_shell_surface,
						zxdg_output,
						output_size: None,
						configured: false,
						progress: 0,
						wl_callback: None
					};

					self.outputs.push(output);
				}
				_ =>
				{}
			}
		}
	}
}

impl Dispatch<zxdg_output_v1::ZxdgOutputV1> for State
{
	type UserData = ();

	fn event(
		&mut self,
		zxdg_output: &zxdg_output_v1::ZxdgOutputV1,
		event: zxdg_output_v1::Event,
		_: &(),
		connection_handle: &mut ConnectionHandle,
		queue_handle: &QueueHandle<Self>
	)
	{
		let output = self
			.outputs
			.iter_mut()
			.find(|output| output.zxdg_output == *zxdg_output)
			.unwrap();

		// TODO handle logical postion to allow spanned backgrounds
		if let zxdg_output_v1::Event::LogicalSize { width, height } = event
		{
			let output_size = (width, height);

			output.output_size = Some(output_size);

			output
				.layer_shell_surface
				.set_size(connection_handle, width as u32, height as u32);

			// anchor to top left
			output.layer_shell_surface.set_anchor(
				connection_handle,
				zwlr_layer_surface_v1::Anchor::Top.union(zwlr_layer_surface_v1::Anchor::Left)
			);

			// prevent being moved to accommodate other surfaces
			output.layer_shell_surface.set_exclusive_zone(connection_handle, -1);

			let mut file = tempfile::tempfile().unwrap();

			draw::draw(&mut file, output_size, &mut output.progress);

			let buffer = draw::create_wl_buffer(
				self.wl_shm.as_ref().unwrap(),
				connection_handle,
				queue_handle,
				&mut file,
				output_size
			)
			.unwrap();

			// commit the surface (layer surface events will not occur until commit)
			output.wl_surface.commit(connection_handle);

			output.buffers.push((buffer, file, true));
			output.wl_callback = Some(output.wl_surface.frame(connection_handle, queue_handle, ()).unwrap());
		}
	}
}

impl Dispatch<wl_callback::WlCallback> for State
{
	type UserData = ();

	fn event(
		&mut self,
		wl_callback: &wl_callback::WlCallback,
		event: wl_callback::Event,
		_: &(),
		connection_handle: &mut ConnectionHandle,
		queue_handle: &QueueHandle<Self>
	)
	{
		let output = self
			.outputs
			.iter_mut()
			.find(|output| *output.wl_callback.as_ref().expect("Callback was none") == *wl_callback)
			.unwrap();

		if let wl_callback::Event::Done {
			callback_data: time_delta_ms
		} = event
		{
			if output.progress == 0
			{
				println!("{}", time_delta_ms);
			}

			let wl_surface = &output.wl_surface;

			let output_size = output.output_size.unwrap();

			output.wl_callback = Some(output.wl_surface.frame(connection_handle, queue_handle, ()).unwrap());

			// find the first free buffer
			match output.buffers.iter_mut().find(|buffer| buffer.2)
			{
				// free buffer found
				Some(buffer) =>
				{
					buffer.1.rewind().unwrap();
					buffer.1.set_len(0).unwrap();

					draw::draw(&mut buffer.1, output_size, &mut output.progress);

					wl_surface.damage(connection_handle, 0, 0, output_size.0, output_size.1);
					wl_surface.attach(connection_handle, Some(&buffer.0), 0, 0);
					wl_surface.commit(connection_handle);
				}
				// no buffer free
				None =>
				{
					let mut file = tempfile::tempfile().unwrap();

					draw::draw(&mut file, output_size, &mut output.progress);

					let wl_buffer = draw::create_wl_buffer(
						self.wl_shm.as_ref().unwrap(),
						connection_handle,
						queue_handle,
						&mut file,
						output_size
					)
					.unwrap();

					wl_surface.damage(connection_handle, 0, 0, output_size.0, output_size.1);
					wl_surface.attach(connection_handle, Some(&wl_buffer), 0, 0);
					wl_surface.commit(connection_handle);

					output.buffers.push((wl_buffer, file, false));
				}
			}
		}
	}
}

impl Dispatch<zwlr_layer_surface_v1::ZwlrLayerSurfaceV1> for State
{
	type UserData = ();

	fn event(
		&mut self,
		layer_shell_surface: &zwlr_layer_surface_v1::ZwlrLayerSurfaceV1,
		event: zwlr_layer_surface_v1::Event,
		_: &(),
		conn: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
		let output = self
			.outputs
			.iter_mut()
			.find(|output| output.layer_shell_surface == *layer_shell_surface)
			.unwrap();

		if let zwlr_layer_surface_v1::Event::Configure { serial, .. } = event
		{
			layer_shell_surface.ack_configure(conn, serial);

			if !output.configured
			{
				let surface = &output.wl_surface;

				surface.attach(conn, Some(&output.buffers[0].0), 0, 0);
				surface.commit(conn);

				output.buffers[0].2 = false;
			}
		}
	}
}

impl Dispatch<wl_buffer::WlBuffer> for State
{
	type UserData = ();

	fn event(
		&mut self,
		wl_buffer: &wl_buffer::WlBuffer,
		event: wl_buffer::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
		let mut buffer = None;

		for output in &mut self.outputs
		{
			buffer = output.buffers.iter_mut().find(|buffer| buffer.0 == *wl_buffer);

			if buffer.is_some()
			{
				break;
			}
		}

		if let wl_buffer::Event::Release = event
		{
			buffer.unwrap().2 = true;
		}
	}
}

impl Dispatch<xdg_wm_base::XdgWmBase> for State
{
	type UserData = ();

	fn event(
		&mut self,
		wm_base: &xdg_wm_base::XdgWmBase,
		event: xdg_wm_base::Event,
		_: &Self::UserData,
		conn: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
		if let xdg_wm_base::Event::Ping { serial } = event
		{
			wm_base.pong(conn, serial);
		}
	}
}

// TODO handle WlDisplay events (delet_id)
impl Dispatch<wl_display::WlDisplay> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_display::WlDisplay,
		_: wl_display::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<zxdg_output_manager_v1::ZxdgOutputManagerV1> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &zxdg_output_manager_v1::ZxdgOutputManagerV1,
		_: zxdg_output_manager_v1::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<wl_output::WlOutput> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_output::WlOutput,
		_: wl_output::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<wl_compositor::WlCompositor> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_compositor::WlCompositor,
		_: wl_compositor::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<wl_surface::WlSurface> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_surface::WlSurface,
		_: wl_surface::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<wl_shm::WlShm> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_shm::WlShm,
		_: wl_shm::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<wl_shm_pool::WlShmPool> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &wl_shm_pool::WlShmPool,
		_: wl_shm_pool::Event,
		_: &Self::UserData,
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

impl Dispatch<zwlr_layer_shell_v1::ZwlrLayerShellV1> for State
{
	type UserData = ();

	fn event(
		&mut self,
		_: &zwlr_layer_shell_v1::ZwlrLayerShellV1,
		_: zwlr_layer_shell_v1::Event,
		_: &(),
		_: &mut ConnectionHandle,
		_: &QueueHandle<Self>
	)
	{
	}
}

// TODO
#[cfg(test)]
mod tests
{
	#[test]
	fn it_works()
	{
		let result = 2 + 2;
		assert_eq!(result, 4);
	}
}
